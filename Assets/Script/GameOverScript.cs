﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    public void clickOnReplay()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void clickOnMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
