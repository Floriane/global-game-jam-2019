﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour
{
    public int life;
    public int nbEntry;
    public bool indestructible;
    public bool[] entry = new bool[4];
    public bool generate;
    public int couldown = 5;
    public int couldownCounter = 1;

    public GameObject themSelfCard;

    public bool Action()
    {
        bool generated = false;
        if(generate)
        {
            if(couldownCounter <= 0)
            {
                generated = true;
                couldownCounter = couldown;
            }
            else
            {
                couldownCounter--;
            }
        }

        return generated;
    }

    private void OnMouseDown()
    {
        Debug.Log("Etape 1");
        GameObject rotation = SortManager.instance.getCurrentSort();
        RotationScript rotationScript = rotation.GetComponent<RotationScript>();
        if(rotationScript != null)
        {
            Debug.Log("Etape 8");
            int tmpI = (int)((transform.position.y - 2.5f) / (-2.5f));
            int tmpJ = (int)((transform.position.x + 11.5f) / 2.5f);
            GameScript.instance.RotationCard(tmpI, tmpJ);
        }
    }

    public void  Rotate()
    {
        Debug.Log(entry[0]);
        Debug.Log(entry[1]);
        Debug.Log(entry[2]);
        Debug.Log(entry[3]);
        bool tmp = entry[0];

        entry[0] = entry[1];
        entry[1] = entry[2];
        entry[2] = entry[3];
        entry[3] = tmp;

        Debug.Log(entry[0]);
        Debug.Log(entry[1]);
        Debug.Log(entry[2]);
        Debug.Log(entry[3]);
    }
}