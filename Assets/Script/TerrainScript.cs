﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainScript : MonoBehaviour
{
    public int i;
    public int j;

    private void OnMouseDown()
    {
        GameObject prefab = BuildManagerScript.instance.getCurrentCard();
        if(prefab != null)
        {
            GameScript.instance.SetCardToCards(prefab, i, j, transform.position, transform.rotation);
        }
        Debug.Log("Click terrain");
    }
}
