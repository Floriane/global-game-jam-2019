﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    BuildManagerScript buildManager;
    // Start is called before the first frame update
    void Start()
    {
        buildManager = BuildManagerScript.instance;
    }

    
    public void ClickOnIndustryAngle()
    {
        buildManager.setCurrentCard(buildManager.industryAngle);
    }

    public void ClickOnIndustryStraight()
    {
        buildManager.setCurrentCard(buildManager.industryStraight);
    }

    public void ClickOnRoadAngle()
    {
        buildManager.setCurrentCard(buildManager.roadAngle);
    }

    public void ClickOnRoadCross()
    {
        buildManager.setCurrentCard(buildManager.roadCross);
    }

    public void ClickOnRoadImpass()
    {
        buildManager.setCurrentCard(buildManager.roadImpass);
    }

    public void ClickOnRoadStraight()
    {
        buildManager.setCurrentCard(buildManager.roadStraight);
    }

    public void ClickOnRoadT()
    {
        buildManager.setCurrentCard(buildManager.roadT);
    }
}
