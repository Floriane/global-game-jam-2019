﻿using System.Collections.Generic;
public class Summit
{
    public Point point;
    public List<Point> adjacentePoint;

    public Summit(Point p, List<Point> aP)
    {
        point = p;
        adjacentePoint = aP;
    }

    public override bool Equals(object obj)
    {
        if (obj == null) return false;
        Summit objAsSummit = obj as Summit;
        if (objAsSummit == null) return false;
        else return Equals(objAsSummit);
    }

    public override int GetHashCode()
    {
        return point.x+point.y*1000;
    }

    public bool Equals(Summit other)
    {
        if (other == null) return false;
        return (this.point.Equals(other.point));
    }
}
