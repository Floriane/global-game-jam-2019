﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemisSpwanerScript : MonoBehaviour {

    [SerializeField]
    private Transform ennemiPrefab;

    [SerializeField]
    private Transform spawPoint;

    private float tempsEntreAttaquant = 5f;

    private float decompte = 2f;

	// Update is called once per frame
	void Update () {

        if (decompte <= 0)
        {
            decompte = tempsEntreAttaquant;
            SpawnAttaquant();
        }

        decompte-= Time.deltaTime;
	}

    void SpawnAttaquant()
    {
        Instantiate(ennemiPrefab, spawPoint.position, spawPoint.rotation);
    }
}
