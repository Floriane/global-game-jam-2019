﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
    public static GameScript instance;
    private float levelTimer = 2f;
    private float turnTime = 3f;
    private int numberOfTurns = 0;

    private int[,] spawns = new int[2, 10] { { 1, 3, 4, 7, 7, 9, 12, 12, 12, 13 }, { 2, 1, 2, 0, 2, 1, 0, 1, 2, 0 } };
    private int spawnsCount = 0;

    private List<GameObject> enemies = new List<GameObject>();
    private List<GameObject> allies = new List<GameObject>();
    private GameObject[][] cards = new GameObject[3][];

    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    private GameObject AllyPrefab;
    [SerializeField]
    private GameObject defaultCardPrefab;
    [SerializeField]
    private GameObject defaultTempleCardPrefab;

    [SerializeField]
    private Transform enemiesSpawner0;
    [SerializeField]
    private Transform enemiesSpawner1;
    [SerializeField]
    private Transform enemiesSpawner2;

    private bool construction = true;
    public GameObject shop;
    public GameObject button;
    public GameObject shopSort;
    public GameObject buttonRotation;

    [SerializeField]
    private Text timerText;
    [SerializeField]
    private Text fireText;

    [SerializeField]
    private GameObject fire;
    FireScript fireScript;

    private Graph graph = new Graph();
    private bool stepConstruction = true;


    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        for (int i = 0; i < cards.Length; i++)
        {
            cards[i] = new GameObject[7];
        }

        initCards();
        fireScript = FireScript.instance;
        /*
        Debug.Log("Test graph");
        Point A = new Point(0, 0);
        Point B = new Point(0, 1);
        Point C = new Point(1, 1);
        List<Point> adjA = new List<Point>();
        List<Point> adjB = new List<Point>();
        List<Point> adjC = new List<Point>();
        adjB.Add(A);
        adjC.Add(B);
        adjC.Add(A);
        Summit sA = new Summit(A, adjA);
        Summit sB = new Summit(B, adjB);
        Summit sC = new Summit(C, adjC);
        graph.AddSummit(sA);
        graph.AddSummit(sB);
        graph.AddSummit(sC);

        graph.displayGraph();
        List<Point> res = graph.ShortestWay(A, C);
        Debug.Log(res.Count);
        for (int i = 0; i < res.Count; i++)
        {
            res[i].displayPoint();
        }*/
    }

    void initCards()
    {
        for (int i = 0; i < cards.Length; i++)
        { 
            float tmpY = 2.5f - 2.5f * i;
            float tmpX = -11.5f;
            Vector3 pos = new Vector3(tmpX, tmpY, -0.5f);
            Quaternion rot = new Quaternion();

            GameObject newCard = Instantiate(defaultTempleCardPrefab, pos, rot);
            AddGraphCard(newCard, i, 0);
            cards[i][0] = newCard;
        }
        // on ajoute une entré au temple central
        GameObject temple = cards[1][0];
        CardScript templeScript = temple.GetComponent<CardScript>();
        templeScript.entry[1] = true;
        templeScript.nbEntry = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (!construction)
        {
            if (levelTimer <= 0) // prochain tour
            {
                Debug.Log("TOUr§§");
                numberOfTurns++;
                levelTimer = turnTime;
                // On fait les actions du tour pour tous les objets dependants du game menager
                // On supprime tous les ennemis qui n'éxiste plus
                CleanEnemies();
                // On fait agir les ennemis
                EnemiesAction();
                // On crée les nouveaux ennemis
                SpawnEnemies();
                // on fait agir les alliés
                AlliesAction();
                // On détruit les quartier à 0pv et on fait agir les quartiers non détruit
                CardsAction();
                //

                fireScript.minusFire(5);
                //fireText.text = fireScript.fireCount.ToString() + "/" + fireScript.maxFire.ToString();
            }
            else
            {
                levelTimer -= Time.deltaTime;

                int affichage = Mathf.CeilToInt(levelTimer);
                if (affichage != 0)
                {
                    timerText.text = Mathf.CeilToInt(levelTimer).ToString();
                }
            }
        }
    }

    void CleanEnemies()
    {
        int cour = 0;
        while (cour < enemies.Count)
        {
            if (enemies[cour] != null)
            {
                cour++;
            }
            else
            {
                enemies.RemoveAt(cour);
                cour = 0;
            }
        }
    }
    void AlliesAction()
    {
        for (int i = 0; i < allies.Count; i++)
        {
            if (allies[i] != null)
            {
                AllyScript allyScript = allies[i].GetComponent<AllyScript>();
                if (allyScript != null)
                {
                    if (allyScript.Action(graph))
                    {
                        Destroy(allies[i]);
                        allies.RemoveAt(i);             
                    }
                }
                else
                {
                    Debug.Log("Pas de scrypt pour l'allié");
                }
            }
            else
            {
                Debug.Log("L'allié est null(à été supprimé) mais est encore présent dans la liste");
            }
        }
    }
    void EnemiesAction()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null)
            {
                EnemyScript enemySript = enemies[i].GetComponent<EnemyScript>();
                if (enemySript != null)
                {
                    GameObject card = cards[enemySript.row][enemySript.column];
                    enemySript.Action(card);
                }
                else
                {
                    Debug.Log("Pas de scrypt pour l'ennemie");
                }
            }
            else
            {
                Debug.Log("L'ennemie est null(à été supprimé) mais est encore présent dans la liste");
            }
        }
    }

    void CardsAction()
    {

        for (int i = 0; i < cards.Length; i++)
        {
            for (int j = 0; j < cards[i].Length; j++)
            {
                if (cards[i][j] != null)
                {
                    CardScript cardScript = cards[i][j].GetComponent<CardScript>();
                    if (cardScript != null)
                    {
                        if((!cardScript.indestructible) && (cardScript.life  <= 0))
                        {
                            GameObject newCard = Instantiate(defaultCardPrefab, cards[i][j].transform.position, Quaternion.identity);
                            Destroy(cards[i][j]);
                            Point p = new Point(i, j);
                            graph.RemooveSummit(p);
                            cards[i][j] = newCard;
                        }
                        else
                        {
                            if(cardScript.Action())
                            {
                                SpawnAlly(i, j);
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("Pas de scrypt pour la carte");
                    }
                }
                else
                {
                    Debug.Log("la carte est null");
                }
            }

        }
    }


    void SpawnEnemies()
    {
        while ((spawnsCount<(spawns.Length/2)) && (numberOfTurns==spawns[0,spawnsCount]))
        {
            SpawnEnemy(spawns[1, spawnsCount]);
            spawnsCount++;
        }
    }

    void SpawnEnemy(int row)
    {
        Transform cour;
        switch (row)
        {
            case 0:
                cour = enemiesSpawner0;
                break;
            case 1:
                cour = enemiesSpawner1;
                break;
            case 2:
                cour = enemiesSpawner2;
                break;

            default:
                cour = null;
                break;
        }
        GameObject enemy = Instantiate(enemyPrefab, cour.position, cour.rotation);
        EnemyScript enemySript = enemy.GetComponent<EnemyScript>();
        if (enemySript != null)
        {
            enemySript.setPos(row, 6);
        }
        else
        {
            Debug.Log("Pas de scrypt pour l'ennemie");
        }
        enemies.Add(enemy);
    }

    void SpawnAlly(int i, int j)
    {
        float tmpY = 2.5f - 2.5f * i;
        float tmpX = -11.5f + 2.5f * j;
        Vector3 pos = new Vector3(tmpX, tmpY, -0.6f);
        Quaternion rot = new Quaternion();
        GameObject newAlly = Instantiate(AllyPrefab, pos, rot);
        AllyScript allyScript = newAlly.GetComponent<AllyScript>();
        allyScript.InitAlly(graph, i, j);
        allies.Add(newAlly);
    }

    public void SetCardToCards(GameObject cardPrefab, int i, int j, Vector3 position, Quaternion rotation)
    {
        position[2] = -0.5f;
        if(cards[i][j] != null)
        {
            Destroy(cards[i][j]);
            Point p = new Point(i, j);
            graph.RemooveSummit(p);
        }

        GameObject card = Instantiate(cardPrefab, position, rotation);
        AddGraphCard(card, i, j);
        cards[i][j] = card;
    }

    private void AddGraphCard(GameObject card, int i, int j)
    {

        Point p = new Point(i, j);
        List<Point> adj = new List<Point>();
        CardScript cardScript = card.GetComponent<CardScript>();
        if (cardScript != null)
        {
            // teste accés voisin
            if ((i > 0) && (cards[i - 1][j] != null)) // il existe une carte au nord
            {
                CardScript cardScriptN = cards[i - 1][j].GetComponent<CardScript>();
                if ((cardScript.entry[0]) && (cardScriptN.entry[2])) // les routes sont reliées
                {
                    Point pN = new Point(i - 1, j);
                    adj.Add(pN);
                }
            }
            if ((j < 6) && (cards[i][j + 1] != null)) // il existe une carte à l'est
            {
                CardScript cardScriptE = cards[i][j + 1].GetComponent<CardScript>();
                if ((cardScript.entry[1]) && (cardScriptE.entry[3])) // les routes sont reliées
                {
                    Point pE = new Point(i, j + 1);
                    adj.Add(pE);
                }
            }
            if ((i < 2) && (cards[i + 1][j] != null)) // il existe une carte au sud
            {
                CardScript cardScriptS = cards[i + 1][j].GetComponent<CardScript>();
                if ((cardScript.entry[2]) && (cardScriptS.entry[0])) // les routes sont reliées
                {
                    Point pS = new Point(i + 1, j);
                    adj.Add(pS);
                }
            }
            if ((j > 0) && (cards[i][j - 1] != null)) // il existe une carte à l'ouest
            {
                CardScript cardScriptO = cards[i][j - 1].GetComponent<CardScript>();
                if ((cardScript.entry[3]) && (cardScriptO.entry[1])) // les routes sont reliées
                {
                    Point pO = new Point(i, j - 1);
                    adj.Add(pO);
                }
            }
            Summit s = new Summit(p, adj);
            graph.AddSummit(s);
        }
    }



    public void FinConstruction()
    {
        construction = false;
        Destroy(shop);
        Destroy(button);
        Destroy(buttonRotation);
        shopSort.SetActive(true);
        BuildManagerScript.instance.setCurrentCard(null);
        SortManager.instance.setCurrentSort(null);

        for (int i = 0; i < cards.Length; i++)
        {
            for (int j = 0; j < cards[i].Length; j++)
            {
                if (cards[i][j] == null)
                {
                    float tmpY = 2.5f - 2.5f * i;
                    float tmpX = -11.5f + 2.5f * j;
                    Vector3 pos = new Vector3(tmpX, tmpY, -0.5f);
                    Quaternion rot = new Quaternion();

                    GameObject newCard = Instantiate(defaultCardPrefab, pos, rot);
                    AddGraphCard(newCard, i, j);
                    cards[i][j] = newCard;
                }
            }
        }
    }

    public void RotationCard(int i, int j)
    {
        cards[i][j].transform.Rotate(0, 0, 90f);
        Point p = new Point(i, j);
        graph.RemooveSummit(p);
        CardScript scriptCard = cards[i][j].GetComponent<CardScript>();
        scriptCard.Rotate();
        AddGraphCard(cards[i][j], i, j);
        if( !stepConstruction)
        {
            fireScript.minusFire(10);
        }
    }
}
