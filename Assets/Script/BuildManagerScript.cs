﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManagerScript : MonoBehaviour
{
    public static BuildManagerScript instance ;
    private GameObject currentCard;

    public GameObject industryAngle;
    public GameObject industryStraight;
    public GameObject roadAngle;
    public GameObject roadCross;
    public GameObject roadImpass;
    public GameObject roadStraight;
    public GameObject roadT;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            currentCard = null;
        }
        
    }

    public GameObject getCurrentCard()
    {
        return currentCard;
    }

    public void setCurrentCard(GameObject newCurrentCard)
    {
        currentCard = newCurrentCard;
    }
}
