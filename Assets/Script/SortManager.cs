﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortManager : MonoBehaviour
{
    public static SortManager instance;
    private GameObject currentSort ;
    public GameObject fireBall;
    public GameObject rotation;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    public GameObject getCurrentSort()
    {
        return currentSort;
    }

    public void setCurrentSort(GameObject newCurrentSort)
    {
        currentSort = newCurrentSort;
    }

    public void ClickOnRotation()
    {
        BuildManagerScript.instance.setCurrentCard(null);
        setCurrentSort(rotation);
    }
}
