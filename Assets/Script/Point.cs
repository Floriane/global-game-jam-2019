﻿using UnityEngine;

public class Point
{
    public int x;
    public int y;

    public Point(int nX, int nY)
    {
        x = nX;
        y = nY;
    }

    public override bool Equals(object obj)
    {
        if (obj == null) return false;
        Point objAsPoint = obj as Point;
        if (objAsPoint == null) return false;
        else return Equals(objAsPoint);
    }
    public override int GetHashCode()
    {
        return x+1000*y;
    }

    public bool Equals(Point other)
    {
        if (other == null) return false;
        return ((this.x == other.x) && (this.y == other.y));
    }

    public void displayPoint()
    {
        Debug.Log("P: ");
        Debug.Log("x = ");
        Debug.Log(x);
        Debug.Log("y = ");
        Debug.Log(y);
        Debug.Log(" :P");
    }
}
