﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyScript : MonoBehaviour
{

    public int speed = 3;
    private int wood = 20;

    private Vector3 objectif;
    private bool moove = true;

    public int row = 0;
    public int column = 0;

    void Start()
    {
        
    }

    public void InitAlly(Graph graph, int row, int column)
    {
        UpdateObjectif(graph);
        setPos(row, column);
    }

    public void setPos(int newRow, int newColumn)
    {
        row = newRow;
        column = newColumn;
    }

    private void Update()
    {
        if (moove)
        {
            Vector3 direction = objectif - transform.position;
            if (direction.magnitude < 0.2)
            {
                Debug.Log("m<0.2");
                moove = false;
            }
            else
            {
                transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
            }

        }
    }

    private void UpdateObjectif(Graph graph)
    {
        //objectif[0] = objectif[0] - 2.5f;
        Point temple = new Point(1, 0);
        Point pos = new Point(row, column);
        List<Point> way = graph.ShortestWay(pos, temple);
        if(way == null)
        {
            moove = false;
        }
        else
        {
            if(way.Count == 0)
            {
                moove = false;
            }
            else
            {
                moove = true;
                Point obj = way[0];
                setPos(obj.x, obj.y);
                float tmpY = 2.5f - 2.5f * obj.x;
                float tmpX = -11.5f + 2.5f * obj.y;
                objectif = new Vector3(tmpX, tmpY, -0.6f);
            }
        }
    }

    public bool Action(Graph graph)
    {
        bool retour = false;
        if (column != 0)
        {
            UpdateObjectif(graph);
        }
        else
        {
            // j'ajoute du bois
            FireScript.instance.plusFire(wood);
            // je me detruit
            retour = true;
        }
        return retour;
    }
}
