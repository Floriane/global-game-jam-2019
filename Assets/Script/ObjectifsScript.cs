﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectifsScript : MonoBehaviour {

    public static Transform[] objectifs ;

    private void Awake()
    {
        objectifs = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            objectifs[i] = transform.GetChild(i);
        }
    }
}
