﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyScript : MonoBehaviour {

    public int speed = 3;

    public int life = 5;
    public int damage = 1;

    private Vector3 objectif;
    private bool moove = true;

    public int row = 0;
    public int column = 0;

    private ParticulEffectScript particulEffectScript;

    public GameObject themSelfObject;

	void Start ()
    {
        objectif = transform.position;
        objectif[0] = objectif[0] - 2.65f;
        particulEffectScript = ParticulEffectScript.instance;
    }

    public void setPos(int newRow, int newColumn)
    {
        row = newRow;
        column = newColumn;
    }

    private void Update()
    {
        if(moove)
        {
            Vector3 direction = objectif - transform.position;
            if (direction.magnitude < 0.2)
            {
                Debug.Log("m<0.2");
                moove = false;
                column = column - 1;
            }
            else
            {
                transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
            }            
        }
    }

    private void UpdateObjectif()
    {
        objectif[0] = objectif[0] - 2.5f;
    }

    public void Action(GameObject card)
    {
        Debug.Log("ACTION§§");
        if (column != 0)
        {
            if (card != null)
            {
                CardScript cardSript = card.GetComponent<CardScript>();
                if (cardSript != null)
                {
                    if (cardSript.life <= 0)
                    {
                        UpdateObjectif();
                        moove = true;
                    }
                    else
                    {
                        Debug.Log("a arret");
                        cardSript.life = cardSript.life - damage;
                        particulEffectScript.Fight(transform.position);
                    }
                }
                else
                {
                    Debug.Log("Pas de scrypt pour la carte lors de l'attaque ennemie");
                }
            }
            else
            {
                Debug.Log("attaque sur case vide");
            }
        }
        else
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    private void OnMouseDown()
    {
        Debug.Log("Click fire - 3");
        GameObject sort = SortManager.instance.getCurrentSort();
        FireBallScript fireBallScript = sort.GetComponent<FireBallScript>();

        if(sort != null)
        {
            fireBallScript.Action(themSelfObject);
        }
    }
}
