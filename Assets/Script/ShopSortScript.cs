﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSortScript : MonoBehaviour
{
    private SortManager sortManager ;

    void Start()
    {
        sortManager = SortManager.instance;
    }

    public void ClickOnFireBall()
    {
        Debug.Log("ClickOnFireBall-1");
        sortManager.setCurrentSort(sortManager.fireBall);
    }
}
