﻿using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    private List<Summit> adjacenceList = new List<Summit>() ;

    public Graph()
    {
    }

    public void displayGraph()
    {
        Debug.Log("G: ");
        foreach (var s in adjacenceList)
        {
            Debug.Log("S: Le point :");
            s.point.displayPoint();
            Debug.Log(" à pour Point adjacent : ");
            foreach (var p in s.adjacentePoint)
            {
                p.displayPoint();
                Debug.Log(" :");
            }
            Debug.Log("S");

        }
        Debug.Log(" :G");
    }
    private int IndexOfPoint(Point a)
    {
        int posA = -1;
        for (int i = 0; i < adjacenceList.Count; i++)
        {
            if (adjacenceList[i].point.Equals(a))
            {
                posA = i;
            }
        }
        return posA;
    }
    public List<Point> ShortestWay(Point a, Point b)
    {
        if(a.Equals(b))
        {
            return new List<Point>();
        }
        int posA = IndexOfPoint(a);
        int posB = IndexOfPoint(b);
        List<Point> way = new List<Point>();
        Point pointCour;
        int posPoint= -1;
        int posN = -1;
        int inf = 10000;
        int posMin = -1;
        int min = -1;
        List<int> distances = new List<int>();
        List<Point> alreadyUse = new List<Point>();
        List<int> prec = new List<int>();
        for (int i = 0; i < adjacenceList.Count; i++)
        {
            distances.Add(inf);
            prec.Add(-1);
        }
        distances[posA] = 0;
        for (int i = 0; i < adjacenceList.Count; i++)
        {
            //on prend le point de plus petite distance
            min = inf;
            posMin = -1;
            for (int j = 0; j < adjacenceList.Count; j++)
            {
                if ((distances[j] <= min) && (!alreadyUse.Contains(adjacenceList[j].point)))
                {
                    min = distances[j];
                    posMin = j;
                }
            }
            // parcour de la liste des voisins du min
            foreach (var point in adjacenceList[posMin].adjacentePoint)
            {
                if(!alreadyUse.Contains(point))
                {
                    posN = IndexOfPoint(point);
                    if ((distances[posMin] + 1) < distances[posN])
                    {
                        distances[posN] = distances[posMin] + 1;
                        prec[posN] = posMin;
                    }
                }
            }
            alreadyUse.Add(adjacenceList[posMin].point);

        }
        bool accessible = true;
        pointCour = b;
        while(accessible && !pointCour.Equals(a))
        {
            way.Insert(0,pointCour);
            posPoint = IndexOfPoint(pointCour);
            if(prec[posPoint] == -1)
            {
                accessible = false;
            }
            else
            {
                pointCour = adjacenceList[prec[posPoint]].point;
            }
        }
        if(!accessible)
        {
            way = null;
        }
        return way;
    }

    public void RemooveSummit(Point p)
    {
        Summit s = null;
        foreach (var summit in adjacenceList)
        {
            if(!summit.point.Equals(p))
            {
                if (summit.adjacentePoint.Contains(p))
                {
                    summit.adjacentePoint.Remove(p);
                }
            }
            else
            {
                s = summit;
            }
        }
        adjacenceList.Remove(s);
    }

    public void AddSummit(Summit newSummit)
    {
        foreach (var summit in adjacenceList)
        {
            if(newSummit.adjacentePoint.Contains(summit.point))
            {
                summit.adjacentePoint.Add(newSummit.point);
            }
        }
        adjacenceList.Add(newSummit);
    }
}
