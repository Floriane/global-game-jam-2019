﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulEffectScript : MonoBehaviour
{
    public static ParticulEffectScript instance;
    public ParticleSystem fightEffect;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void Fight(Vector3 position)
    {
        position[2] = -1f;
        Debug.Log(position);
        ParticleSystem newParticleSystem = Instantiate( fightEffect, position, Quaternion.identity ) as ParticleSystem;
        newParticleSystem.Play();

        Debug.Log("particule effect");

        // Destruction programmée
       // Destroy( newParticleSystem.gameObject, newParticleSystem.startLifetime);
    }
}
