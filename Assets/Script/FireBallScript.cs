﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallScript : MonoBehaviour
{
   public int cost = 10;

   public void Action(GameObject ennemy)
    {
        Debug.Log("TEst fireBall - 5");
        if(FireScript.instance.fireCount - cost >= 0)
        {
            Debug.Log("destroy");
            Destroy(ennemy);
            FireScript.instance.minusFire(cost);
        }
    }
}
