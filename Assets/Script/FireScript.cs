﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FireScript : MonoBehaviour
{
    public static FireScript instance;
    public int fireCount = 75;
    public int maxFire = 75;
    public Text info;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    public void minusFire(int minus)
    {
        fireCount = fireCount - minus;
        if(fireCount == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        UploadText();
    }

    public void plusFire(int plus)
    {
        fireCount = fireCount + plus;
        if(fireCount > maxFire)
        {
            fireCount = maxFire;
        }
        UploadText();
    }

    private void UploadText()
    {
        info.text = fireCount.ToString() + "/" + maxFire.ToString();
    }
}
